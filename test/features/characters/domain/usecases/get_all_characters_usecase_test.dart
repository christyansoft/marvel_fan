import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:marvel_fan/core/errors/failures.dart';
import 'package:marvel_fan/core/usecase/usecase.dart';
import 'package:marvel_fan/features/characters/domain/entities/character_entity.dart';
import 'package:marvel_fan/features/characters/domain/repositories/icharacter_repository.dart';
import 'package:marvel_fan/features/characters/domain/usecases/get_all_characters_usecase.dart';
import 'package:mocktail/mocktail.dart';

class MockCharacterRepository extends Mock implements ICharacterRepository {}

void main() {
  late GetAllCharactersUsecase usecase;
  late ICharacterRepository repository;

  setUp(() {
    repository = MockCharacterRepository();
    usecase = GetAllCharactersUsecase(repository);
  });

  final tCharacter = CharacterEntity(
    id: 1017100,
    name: 'A-Bomb (HAS)',
    description:
        'Rick Jones has been Hulk\'s best bud since day one, but now he\'s more than a friend...he\'s a teammate! Transformed by a Gamma energy explosion, A-Bomb\'s thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction! ',
    thumbnail: Thumbnail(
      path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16',
      extension: 'jpg',
    ),
  );

  final noParams = NoParams();

  test('should get a character entity when the repository is called', () async {
    //Arrange
    when(() => repository.getAllCharacters(noParams))
        .thenAnswer((_) async => Right(tCharacter));

    //Act
    final result = await usecase(noParams);

    //Assert
    expect(result, Right(tCharacter));
    verify(() => repository.getAllCharacters(noParams)).called(1);
  });

  test('should return a ServerFailure when don\'t succeed', () async {
    //Arrange
    when(() => repository.getAllCharacters(noParams))
        .thenAnswer((_) async => Left(ServerFailure()));

    //Act
    final result = await usecase(noParams);

    //Assert
    expect(result, Left(ServerFailure()));
    verify(() => repository.getAllCharacters(noParams)).called(1);
  });

}
