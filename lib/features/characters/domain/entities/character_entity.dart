import 'package:equatable/equatable.dart';

class CharacterEntity extends Equatable {
  final int id;
  final String name;
  final String description;
  final Thumbnail thumbnail;

  CharacterEntity({
    required this.id,
    required this.name,
    required this.description,
    required this.thumbnail,
  });

  String get imageUrl => '${thumbnail.path}/portrait_incredible.jpg';

  @override
  List<Object?> get props => [id, name, description, thumbnail];
}

class Thumbnail extends Equatable {
  final String path;
  final String extension;

  Thumbnail({required this.path, required this.extension});

  @override
  List<Object?> get props => [path, extension];
}
