import 'package:dartz/dartz.dart';
import 'package:marvel_fan/core/errors/failures.dart';
import 'package:marvel_fan/core/usecase/usecase.dart';
import 'package:marvel_fan/features/characters/domain/entities/character_entity.dart';

abstract class ICharacterRepository {
  Future<Either<Failure, CharacterEntity>> getAllCharacters(NoParams noParams);
}
