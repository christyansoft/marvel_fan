import 'package:dartz/dartz.dart';
import 'package:marvel_fan/core/errors/failures.dart';
import 'package:marvel_fan/core/usecase/usecase.dart';
import 'package:marvel_fan/features/characters/domain/entities/character_entity.dart';
import 'package:marvel_fan/features/characters/domain/repositories/icharacter_repository.dart';

class GetAllCharactersUsecase implements Usecase<CharacterEntity, NoParams>{

  final ICharacterRepository repository;

  GetAllCharactersUsecase(this.repository);

  @override
  Future<Either<Failure, CharacterEntity>> call(NoParams params) async {
    return await repository.getAllCharacters(params);
  }


}